<?php
/**
 * @file
 * humanitarian_codebase.domains.inc
 */

/**
 * Implements hook_domain_default_domains().
 */
function humanitarian_codebase_domain_default_domains() {
  $domains = array();
  $domains['wipe-domain-tables'] = 'wipe-domain-tables';
  $domains['ethiopia_localhost_com'] = array(
    'subdomain' => 'ethiopia.localhost.com',
    'sitename' => 'Humanitarian response Ethiopia',
    'scheme' => 'http',
    'valid' => '1',
    'weight' => '3',
    'is_default' => '0',
    'machine_name' => 'ethiopia_localhost_com',
  );
  $domains['kenya_localhost_com'] = array(
    'subdomain' => 'kenya.localhost.com',
    'sitename' => 'humanitarian response',
    'scheme' => 'http',
    'valid' => '1',
    'weight' => '1',
    'is_default' => '0',
    'machine_name' => 'kenya_localhost_com',
  );
  $domains['localhost'] = array(
    'subdomain' => 'localhost',
    'sitename' => 'humanitarian response',
    'scheme' => 'http',
    'valid' => '1',
    'weight' => '-1',
    'is_default' => '1',
    'machine_name' => 'localhost',
  );
  $domains['uganda_localhost_com'] = array(
    'subdomain' => 'uganda.localhost.com',
    'sitename' => 'Humanitarian response Uganda',
    'scheme' => 'http',
    'valid' => '1',
    'weight' => '2',
    'is_default' => '0',
    'machine_name' => 'uganda_localhost_com',
  );

  return $domains;
}

/**
 * Implements hook_domain_conf_default_variables().
 */
function humanitarian_codebase_domain_conf_default_variables() {
  $domain_variables = array();
  $domain_variables['ethiopia_localhost_com'] = array();
  $domain_variables['kenya_localhost_com'] = array(
    'site_name' => 'humanitarian response',
    'site_slogan' => '',
    'site_mail' => 'trollmatt@gmail.com',
    'default_nodes_main' => '10',
    'site_frontpage' => 'node',
    'site_403' => '',
    'site_404' => '',
  );
  $domain_variables['localhost'] = array();
  $domain_variables['uganda_localhost_com'] = array();

  return $domain_variables;
}
